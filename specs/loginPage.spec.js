let loginPage = require('../pages/loginPage.page.js');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1200);

Given('The user is on BitPanda', async function () {
  await browser.waitForAngularEnabled(false);
  await loginPage.getBitPandaPage();
});

Given('The user goes to log in', async function () {
  await loginPage.goToLogin();
});

When('The user fills all log in fields correct', async function () {
  await loginPage.fillAllCorrect();
});

When('The user does not fill all log in fields correctly', async function () {
  await loginPage.fillAllIncorrect();
});

When('The user submits', async function () {
  await loginPage.submit();
});

Then('The user should see the home page', async function () {
  expect(await loginPage.getWelcomeMessage())
    .to.equal("Welcome, Tulio Luz!");
});

Then('The user should see the required fields messages', async function () {
  expect(await loginPage.getEmailRequiredMessage())
    .to.equal("Email is required");
  expect(await loginPage.getPasswordRequiredMessage())
    .to.equal("Password is required");
});

Then('The user should see credentials incorrect message', async function () {
  expect(await loginPage.getIncorrectMessage())
    .to.equal(" Login credentials incorrect ");
});