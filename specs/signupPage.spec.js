let signupPage = require('../pages/signupPage.page.js');

Given('The user goes to sign up', async function () {
  await signupPage.goToSignUp();
});

When('The user fills all register fields correct', async function () {
  await signupPage.fillAllCorrect();
  await signupPage.acceptTermsConditions();
});

When('The user accepts the terms and conditions', async function () {
  await signupPage.acceptTermsConditions();
});

When('The user does not fill all register fields correctly', async function () {
  await signupPage.acceptTermsConditions();
});

Then('The user should see the successful message', async function () {
  expect(await signupPage.getRegisterSuccessMessage())
    .to.equal("Thank you for signing up!");
});

Then('The user should see the required terms messages', async function () {
  expect(await signupPage.getAcceptMessage())
    .to.equal("You have to accept our Terms and Conditions and Privacy Policy in order to use Bitpanda");
});