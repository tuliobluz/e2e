## e2e with Protractor

### Technologies used

I used the technologies below:

* [Protractor](https://www.protractortest.org/#/): Protractor is an end-to-end test framework for Angular and AngularJS applications. Protractor runs tests against your application running in a real browser, interacting with it as a user would;
* [Page Objects](https://www.protractortest.org/#/page-objects): Page Objects help you write cleaner tests by encapsulating information about the elements on your application page. A Page Object can be reused across multiple tests, and if the template of your application changes, you only need to update the Page Object;
* [CucumberJS](https://github.com/cucumber/cucumber-js): Cucumber is a tool for running automated tests written in plain language. Because they're written in plain language, they can be read by anyone on your team. Because they can be read by anyone, you can use them to help improve communication, collaboration and trust on your team;

### Requirements

- [Node.js](https://nodejs.org/en/download/) installed;
- Its need to have the [Java Development Kit (JDK)](https://www.oracle.com/technetwork/java/javase/downloads/index.html) installed to run the standalone Selenium Server.

### To set up all environment

- Run ```npm install``` to install dependencies

## e2e

https://www.bitpanda.com/en

### Folders Structures

* ```e2e```
    * ```helper``` Folder to helpers
        * ```data.js``` Data structure to use in the tests
    * ```features``` Where feature files should be created
        * ```loginPage.feature```
    * ```pages``` Where the page object of tests should be created
        * ```loginPage.page.js```
    * ```specs``` Where the specification of tests should be created
        * ```loginPage.spec.js```

### Install or Update the binaries drivers

- Run ```npm run webdriver-update``` to install/update the binaries drivers

### Running tests

- Start the Selenium Server ```npm run webdriver-start```

- Run the tests ```npm run e2e```

- Just run the tests are done protractor ```npm run e2e -- --cucumberOpts.tags='@pending'``` without pending scenarios

### Headless

If you want to run the tests in your computer and Headless, you just need to use the config file ```protractor.conf-headless.js```

- Run the tests ```npm run e2e-headless```