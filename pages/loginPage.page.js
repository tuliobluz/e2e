let data = require('../helper/data.js');

let LoginPage = function () {
  let loginButton = element(by.css('.nav__primary__listitem .btn.btn--outline.nav__primary__btn')),
    email = element(by.id('email')),
    password = element(by.id('password')),
    submit = element(by.css('.btn.btn-primary.btn-block')),
    welcomeMessage = element(by.css('.user-name.ng-star-inserted')),
    emailRequiredMessage = element(by.xpath('//input[@id="email"]/following-sibling::span[1]')),
    passwordRequiredMessage = element(by.xpath('//input[@id="password"]/following-sibling::span[1]')),
    incorrectMessage = element(by.css('.alert'));

  this.getBitPandaPage = async function () {
    await browser.get('/')
  }
  this.goToLogin = async function () {
    await loginButton.click();
  }
  this.fillAllCorrect = async function () {
    await email.sendKeys(data.user.email);
    await password.sendKeys(data.user.validPassword);
  }
  this.fillAllIncorrect = async function () {
    await email.sendKeys(data.user.email);
    await password.sendKeys(data.user.invalidPassword);
  }
  this.submit = async function () {
    await submit.click();
  }
  this.getWelcomeMessage = function () {
    return welcomeMessage.getText();
  }
  this.getEmailRequiredMessage = function () {
    return emailRequiredMessage.getText();
  }
  this.getPasswordRequiredMessage = function () {
    return passwordRequiredMessage.getText();
  }
  this.getIncorrectMessage = function () {
    return incorrectMessage.getText();
  }
}
module.exports = new LoginPage();