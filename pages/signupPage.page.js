let faker = require('faker');
let data = require('../helper/data.js');

let SignupPage = function () {
  let firstName = faker.name.firstName(),
    lastName = faker.name.lastName(),
    emailUser = faker.internet.email();

  let signupButton = element(by.css('.nav__primary__listitem .btn.btn--solid.nav__primary__btn')),
    name = element(by.id('forename')),
    surname = element(by.id('surname')),
    email = element(by.id('email')),
    password = element(by.id('password')),
    country = element(by.css('.ng-input > input')),
    acceptTerm = element(by.xpath('//input[@id="_terms_accepted_input"]/following-sibling::button')),
    acceptPolicy = element(by.xpath('//input[@id="_privacy_policy_input"]/following-sibling::button')),
    registerSuccessMessage = element(by.css('.font-size-24.text-center.mb-15')),
    acceptMessage = element(by.css('.toast-message.ng-star-inserted'));

  this.goToSignUp = async function () {
    await signupButton.click();
  }
  this.fillAllCorrect = async function () {
    await name.sendKeys(firstName);
    await surname.sendKeys(lastName);
    await email.sendKeys(emailUser);
    await password.sendKeys(data.user.validPassword);
    await country.sendKeys(data.user.country);
    await browser.actions().sendKeys(protractor.Key.ENTER).perform();
  }
  this.acceptTermsConditions = async function () {
    await acceptTerm.click();
    await acceptPolicy.click();
  }
  this.getRegisterSuccessMessage = function () {
    return registerSuccessMessage.getText();
  }
  this.getAcceptMessage = function () {
    return acceptMessage.getText();
  }
}
module.exports = new SignupPage();