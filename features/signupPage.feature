@signupPage
Feature: The user is able to sign up BitPanda
  As a web user,
  The user wants to sign up
  So that the user can access the application

  Background: The user accesses the BitPanda
    Given The user is on BitPanda

  Scenario: The user sign up successfully
    Given The user goes to sign up
    When The user fills all register fields correct
    And The user submits
    Then The user should see the successful message

  Scenario: The user sign up without accept terms
    Given The user goes to sign up
    And The user submits
    Then The user should see the required terms messages

  Scenario: The user sign up without fill required fields
    Given The user goes to sign up
    When The user accepts the terms and conditions
    And The user submits
    Then The user should see the required fields messages