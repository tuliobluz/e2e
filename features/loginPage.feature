@loginPage
Feature: The user is able to log in BitPanda
	As a web user,
	The user wants to log in
	So that the user can use the application

	Background: The user accesses the BitPanda
		Given The user is on BitPanda

	Scenario: The user login successfully
		Given The user goes to log in
		When The user fills all log in fields correct
		And The user submits
		Then The user should see the home page

	Scenario: The user login in unsuccessfully
		Given The user goes to log in
		When The user submits
		Then The user should see the required fields messages

	Scenario: The user login incorrect
		Given The user goes to log in
		When The user does not fill all log in fields correctly
		And The user submits
		Then The user should see credentials incorrect message